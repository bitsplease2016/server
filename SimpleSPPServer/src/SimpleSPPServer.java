import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

  
import javax.bluetooth.*;
import javax.microedition.io.*;
  
/**
* Class that implements an SPP Server which accepts single line of
* message from an SPP client and sends a single line of response to the client.
*/
public class SimpleSPPServer {
	
	
	
	HashMap<String,String> hashMap = new HashMap<String,String>();
	HashMap<String , String> slidersHashMap = new HashMap<String,String>();
	String progress = "";
	//File file = new File("SignalsReceived.txt");
    FileWriter outfile;
    
    public SimpleSPPServer() throws IOException {
    	
    	this.outfile = new FileWriter(new File("SignalsReceived.txt") , false);
    	
    }


    //start server
    /**
     * This starts the server. Creates and waits for a bluetooth connection from a paired device.
     * Once connection is made it enters a loop that recives a string of text through a buffer 
     * from the paired device and then writes to file.
     * 
     * @throws IOException If problem writing to the file or closing
     */
    private void startServer()  throws IOException{
	
    	//Create a UUID for SPP
        UUID uuid = new UUID("1101", true);
        //Create the servicve url
        String connectionString = "btspp://localhost:" + uuid +";name=Sample SPP Server";
        
        //open server url
        StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier)Connector.open( connectionString );
        
        //Wait for client connection
        System.out.println("\nServer Started. Waiting for clients to connect...");
        StreamConnection connection=streamConnNotifier.acceptAndOpen();
  
        RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
        System.out.println("Remote device address: "+dev.getBluetoothAddress());
        System.out.println("Remote device name: "+dev.getFriendlyName(true));
     
        
        //read string from spp client
        InputStream inStream=connection.openInputStream();
        BufferedReader bReader=new BufferedReader(new InputStreamReader(inStream));
        
        String lineRead= null;
        
        
        do {
      
        lineRead=bReader.readLine();
        
        //System.out.println(lineRead);
        if (lineRead.equalsIgnoreCase("closefile"))
        	
        {
        	closeFile(outfile);
        }
     
         if (!(hashMap.get(lineRead) == null))
        {
        System.out.println(hashMap.get(lineRead));
        writeSignalToFile( outfile , hashMap.get(lineRead) );
        }
        
        try {
        	String[] sliderData = lineRead.split(Pattern.quote("."));
        	if (! (slidersHashMap.get(sliderData[0])== null))
        	{
        		progress = sliderData[1];
        		System.out.println(slidersHashMap.get(sliderData[0]) + ": " + progress);
        		
        		
        	}
        		
        	
        	
        
        }
      catch (Exception e)
        {
        }
        
        
      
        
      
        } while (!lineRead.equals("close"));
        
            
             
            
        //send response to spp client
        OutputStream outStream=connection.openOutputStream();
        PrintWriter pWriter=new PrintWriter(new OutputStreamWriter(outStream));
        pWriter.write("Response String from SPP Server\r\n");
        pWriter.flush();
        streamConnNotifier.close();
        pWriter.close();
        
  
    }
    
    /**
     * Reads in an XML file which is stored in the xml directory of project.
     * The XML will contain each buttons signal and name. Also sliders signal and name.
     * It will then put both into a map to be used.
     * 
     */
    private void readFromFile()
    {
    	try {
    		File fXmlFile = new File("xml\\test.xml");
    		System.out.println("C:\\Users\\Leigh\\workspace\\SimpleSPPServer\\xml\\test.xml");	
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fXmlFile);
    		doc.getDocumentElement().normalize();//read about
    		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
    		NodeList nList = doc.getElementsByTagName("buttons");
    		NodeList list = doc.getElementsByTagName("sliders");

    		System.out.println("----------------------------");

    		for (int temp = 0; temp < nList.getLength(); temp++) {

    			Node nNode = nList.item(temp);

    			System.out.println("\nCurrent Element :" + nNode.getNodeName());

    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

    				Element eElement = (Element) nNode;

    				
    				hashMap.put(eElement.getElementsByTagName("name").item(0).getTextContent() , eElement.getElementsByTagName("signal").item(0).getTextContent() );
    				

    			}
    			
    			
    			
    			
    		}
    		for (int temp = 0; temp < list.getLength(); temp++) {

    			Node nNode = list.item(temp);

    			System.out.println("\nCurrent Element :" + nNode.getNodeName());

    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

    				Element eElement = (Element) nNode;

    				
    				slidersHashMap.put(eElement.getElementsByTagName("name").item(0).getTextContent() , eElement.getElementsByTagName("signal").item(0).getTextContent() );
    				
    				

    			}
    		}
    	}
    	     catch (Exception e) {
    		e.printStackTrace();
    	    }
    	
   	
    	
    	  }
    
    /**
     * Writes a signal to a text file
     * 
     * 
     * @param outfile Where signal are written
     * @param signal the signal to write
     * @throws IOException 
     */
    public void writeSignalToFile(FileWriter outfile, String signal) throws IOException
    {
    	
    	outfile.write(signal + System.lineSeparator());
    	
    }
    
    /**
     * Closes the file once app exits.
     * 
     * @param outfile the file to close.
     * @throws IOException
     */
    public void closeFile(FileWriter outfile) throws IOException
    {
    	outfile.close();
    	
    }


    
    public static void main(String[] args) throws IOException  {
    	
    	
       
       
        SimpleSPPServer sampleSPPServer=new SimpleSPPServer();
        sampleSPPServer.readFromFile();
        LocalDevice localDevice = LocalDevice.getLocalDevice();
        //display local device address and name
        System.out.println("Address: "+localDevice.getBluetoothAddress());
        System.out.println("Name: "+localDevice.getFriendlyName());
   
        sampleSPPServer.startServer();
        
       
        
    }
}